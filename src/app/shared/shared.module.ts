import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import {
  MatToolbarModule,
  MatListModule,
  MatIconModule,
  MatButtonModule,
} from '@angular/material';

import { FormTextInputComponent } from './components';

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,

  MatToolbarModule,
  MatListModule,
  MatIconModule,
  MatButtonModule,
];

const COMPONENTS = [
  FormTextInputComponent,
];


@NgModule({
  imports: MODULES,
  declarations: [
    ...COMPONENTS,
  ],
  exports: [
    ...MODULES,
    ...COMPONENTS,
  ]
})
export class SharedModule { }
