import {
  Component,
  Input,
  Output,
  EventEmitter,
  forwardRef
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'app-form-text-input',
  templateUrl: './form-text-input.component.html',
  styleUrls: ['./form-text-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormTextInputComponent),
      multi: true
    }
  ],
})
export class FormTextInputComponent implements ControlValueAccessor {
  @Input() value = '';
  @Input() type = 'text';
  @Input() name: string;
  @Input() label: string;
  @Input() disabled = false;
  @Input() required = false;
  @Input() autocomplete: string;
  @Input() placeholder: string;
  @Output() handleChange = new EventEmitter<string>();

  constructor() { }

  valueChanged(value: string) {
    this.writeValue(value);
  }

  onChange = (value: string) => {};

  onTouched = () => {};

  writeValue(value: string): void {
    if (value !== this.value) {
      this.value = value;
    }
    this.onChange(value);
  }

  registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
