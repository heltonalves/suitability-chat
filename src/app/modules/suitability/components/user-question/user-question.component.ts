import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
} from '@angular/core';
import { Router } from '@angular/router';

import { SuitabilityService } from '../../services/suitability.service';
import {
  SuitabilityChat,
  UserSuitabilityChat,
  SuitabilityAnswer,
} from '../../models';

import { QuestionFormComponent } from '../question-form/question-form.component';
import { QuestionOptionsComponent } from '../question-options/question-options.component';

@Component({
  selector: 'app-user-question',
  templateUrl: './user-question.component.html',
  styleUrls: ['./user-question.component.scss']
})
export class UserQuestionComponent implements OnInit {
  @ViewChild('container', { static: true, read: ViewContainerRef }) entry: ViewContainerRef;

  private respData: UserSuitabilityChat;
  private answerData: SuitabilityChat = {
    context: 'suitability',
    subcontext: 'suitability_newintro',
    answers: {},
  };

  constructor(
    private resolver: ComponentFactoryResolver,
    private suitabilityService: SuitabilityService,
    private router: Router,
  ) {
    this.answer();
  }

  ngOnInit() { }

  answer(): void {
    this.suitabilityService.send(this.answerData).subscribe(
      (resp: UserSuitabilityChat) => {
        this.respData = resp;
        this.parseResponse();

        this.loadComponent();
      },
      err => {
        console.log('Received an error in answer: ');
        console.log(err);
      },
    );
  }

  private loadComponent(): void {
    this.entry.clear();

    if (this.respData.buttons.length > 0) {
      this.loadQuestionOptions(this.respData);
      return;
    }

    if (this.respData.inputs.length > 0) {
      this.loadQuestionForm(this.respData);
      return;
    }
  }

  private loadQuestionOptions(data: UserSuitabilityChat): void {
    this.entry.clear();
    const factory = this.resolver.resolveComponentFactory(QuestionOptionsComponent);
    const componentRef = this.entry.createComponent(factory);
    componentRef.instance.setData = data;
    componentRef.instance.triggerResponse.subscribe(
      (resp: SuitabilityAnswer) => this.setAnswer(resp)
    );
  }

  private loadQuestionForm(data: UserSuitabilityChat): void {
    this.entry.clear();
    const factory = this.resolver.resolveComponentFactory(QuestionFormComponent);
    const componentRef = this.entry.createComponent(factory);
    componentRef.instance.setData = data;
    componentRef.instance.triggerResponse.subscribe(
      (resp: SuitabilityAnswer) => this.setAnswer(resp)
    );
  }

  private setAnswer(answer: SuitabilityAnswer): void {
    this.answerData.answers = {...this.answerData.answers, ...answer};
    this.answer();
  }

  private parseResponse(): void {
    this.suitabilityService.triggerMessages(this.respData.messages);
    this.answerData.id = this.respData.id;
  }

  getProfile(): void {
    this.suitabilityService.finish(this.answerData)
    .subscribe(
      (resp: any) => {
        this.router.navigate(['suitability/profile'], { state: { data: resp } });
      },
      err => {
        console.log('Received an error in getProfile: ');
        console.log(err);
      },
    );
  }
}
