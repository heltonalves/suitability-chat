import {
  Component,
  Output,
  Input,
  EventEmitter,
} from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

import { UserSuitabilityChat, SuitabilityAnswer } from '../../models';
import { SuitabilityService } from '../../services/suitability.service';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})
export class QuestionFormComponent {
  data: UserSuitabilityChat;
  answerForm: FormGroup;
  loading = false;
  label = '';
  placeholder = '';
  type = 'text';

  @Output() triggerResponse: EventEmitter<SuitabilityAnswer> = new EventEmitter();

  @Input()
  set setData(data: UserSuitabilityChat) {
    this.data = data;

    switch (this.data.id) {
      case 'question_name': {
        this.buildName();
        break;
      }
      case 'question_age': {
        this.buildAge();
        break;
      }
      case 'question_income': {
        this.buildIncome();
        break;
      }
      case 'question_email': {
        this.buildEmail();
        break;
      }
    }
  }

  constructor(
    private fb: FormBuilder,
    private suitabilityService: SuitabilityService,
  ) {
    this.answerForm = this.fb.group({
      answer: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.answerForm.valid) {
      this.suitabilityService.triggerMessages({
        owner: 'user',
        value: ((this.data.responses.length)) ?
          `${this.data.responses[0].replace(/{{.*}}/, this.answerForm.get('answer').value)}` :
          this.answerForm.get('answer').value,
      });

      this.triggerResponse.emit({
        [this.data.id]: this.answerForm.get('answer').value,
      });
    }
  }

  isFieldValid(field: string): boolean {
    return !this.answerForm.get(field).valid && this.answerForm.get(field).touched;
  }

  private buildName(): void {
    this.placeholder = 'digite seu nome';
  }

  private buildAge(): void {
    this.placeholder = 'digite sua idade';
    this.type = 'number';
  }

  private buildIncome(): void {
    this.label = 'R$';
    this.type = 'number';
  }

  private buildEmail(): void {
    this.placeholder = 'digite seu email';
    this.type = 'email';
  }
}
