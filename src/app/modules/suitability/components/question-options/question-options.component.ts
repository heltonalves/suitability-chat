import {
  Component,
  Output,
  Input,
  EventEmitter,
} from '@angular/core';

import {
  UserSuitabilityChat,
  SuitabilityAnswer,
  SuitabilityButton,
} from '../../models';

import { SuitabilityService } from '../../services/suitability.service';

@Component({
  selector: 'app-question-options',
  templateUrl: './question-options.component.html',
  styleUrls: ['./question-options.component.scss']
})
export class QuestionOptionsComponent {
  data: UserSuitabilityChat;

  @Output() triggerResponse: EventEmitter<SuitabilityAnswer> = new EventEmitter();

  @Input()
  set setData(data: UserSuitabilityChat) {
    this.data = data;
  }

  constructor(
    private suitabilityService: SuitabilityService,
  ) { }

  selectOption(item: SuitabilityButton): void {
    this.suitabilityService.triggerMessages({
      owner: 'user',
      value: item.label.title,
    });

    this.triggerResponse.emit({
      [this.data.id]: item.value,
    });
  }
}
