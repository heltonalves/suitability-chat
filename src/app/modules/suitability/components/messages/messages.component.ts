import { Component, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { SuitabilityService } from '../../services/suitability.service';
import { SuitabilityMessage } from '../../models';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnDestroy {
  @ViewChild('wrapperMessages', { static: true, read: ElementRef }) messagesContainer: ElementRef;

  subsMessages: Subscription;
  messages: SuitabilityMessage[] = [];

  constructor(
    private suitabilityService: SuitabilityService,
  ) {
    this.subsMessages = this.suitabilityService.messagesCheck$.subscribe((data: SuitabilityMessage | SuitabilityMessage[]) => {
      if (Array.isArray(data)) {
        this.messages = this.messages.concat(data);
      } else {
        this.messages.push(data);
      }

      this.scrollBottom();
    });
  }

  scrollBottom() {
    const ele = this.messagesContainer.nativeElement;
    setTimeout(() => {
      window.scrollTo(0, ele.offsetHeight);
    }, 50);
  }

  ngOnDestroy(): void {
    this.subsMessages.unsubscribe();
  }
}
