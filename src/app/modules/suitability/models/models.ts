export interface SuitabilityMessage {
  owner?: string;
  type?: string;
  value: string;
}

export interface SuitabilityButton {
  value: string;
  label: {
    title: string
  };
}

export interface SuitabilityInput {
  type: string;
  mask: string;
}

export interface UserSuitabilityChat {
  id: string;
  messages?: SuitabilityMessage[];
  buttons?: SuitabilityButton[];
  inputs?: SuitabilityInput[];
  responses?: string[];
}

export interface SuitabilityAnswer {
  [key: string]: string;
}

export interface SuitabilityChat {
  id?: string;
  context: string;
  subcontext: string;
  answers?: SuitabilityAnswer;
}
