import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SuitabilityService } from './suitability.service';

describe('SuitabilityService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
  }));

  it('should be created', () => {
    const service: SuitabilityService = TestBed.get(SuitabilityService);
    expect(service).toBeTruthy();
  });
});
