import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';

import {
  SuitabilityChat,
  SuitabilityMessage,
  UserSuitabilityChat,
} from '../models';


@Injectable({
  providedIn: 'root'
})
export class SuitabilityService {
  private messagesCheck = new Subject<any>();
  public messagesCheck$ = this.messagesCheck.asObservable();

  constructor(
    private http: HttpClient,
  ) { }

  triggerMessages(data: SuitabilityMessage | SuitabilityMessage[]) {
    this.messagesCheck.next(data);
  }

  send(data: SuitabilityChat): Observable<UserSuitabilityChat> {
    return this.http.post<UserSuitabilityChat>('api/conversation/message', data);
  }

  finish(data: SuitabilityChat): Observable<any> {
    return this.http.post<any>('api/suitability/finish', data);
  }

  saveUserToken(token: string): void {
    localStorage.setItem('warren.accounts.accessToken', token);
  }
}
