import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatComponent, ProfileComponent } from './containers';


const routes: Routes = [
  {
    path: '',
    component: ChatComponent,
  },
  {
    path : 'profile',
    component : ProfileComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuitabilityRoutingModule { }
