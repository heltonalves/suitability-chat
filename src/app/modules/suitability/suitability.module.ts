import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { SuitabilityRoutingModule } from './suitability-routing.module';

import {
  HeaderComponent,
  MessagesComponent,
  UserQuestionComponent,
  QuestionOptionsComponent,
  QuestionFormComponent,
} from './components';

import {
  ChatComponent,
  ProfileComponent,
} from './containers';


@NgModule({
  declarations: [
    ChatComponent,
    MessagesComponent,
    UserQuestionComponent,
    QuestionOptionsComponent,
    QuestionFormComponent,
    ProfileComponent,
    HeaderComponent,
  ],
  imports: [
    SharedModule,
    SuitabilityRoutingModule,
  ],
  entryComponents: [
    QuestionOptionsComponent,
    QuestionFormComponent,
  ],
})
export class SuitabilityModule { }
