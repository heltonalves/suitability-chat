import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  data: object;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.paramMap.pipe(
      map(() => window.history.state)
    ).subscribe(state => {
      if (!state.data) {
        this.router.navigate(['/']);
      }

      this.data = state.data;
    });
  }
}
