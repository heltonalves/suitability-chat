# Descobrindo seu Perfil

Um chat usando o Bot da Warren, que se trata de uma análise de perfil de investimento do usuário através de perguntas e respostas.

## Installing dependencies

Requires Node 11 or later.
To install dependencies by npm.

```
$ npm install
```

## Starting the application

### Development

To run the application in development mode:

```
$ npm start

> suitability-chat@0.0.0 start /Users/heltonalves/Projects/suitability-chat
> ng serve --proxy-config proxy.conf.json

 10% building 3/3 modules 0 activeℹ ｢wds｣: Project is running at http://localhost:4200/webpack-dev-server/
ℹ ｢wds｣: webpack output is served from /
ℹ ｢wds｣: 404s will fallback to //index.html

chunk {main} main.js, main.js.map (main) 11.8 kB [initial] [rendered]
chunk {modules-suitability-suitability-module} modules-suitability-suitability-module.js, modules-suitability-suitability-module.js.map (modules-suitability-suitability-module) 2.51 MB  [rendered]
chunk {polyfills} polyfills.js, polyfills.js.map (polyfills) 251 kB [initial] [rendered]
chunk {runtime} runtime.js, runtime.js.map (runtime) 8.98 kB [entry] [rendered]
chunk {styles} styles.js, styles.js.map (styles) 407 kB [initial] [rendered]
chunk {vendor} vendor.js, vendor.js.map (vendor) 4.58 MB [initial] [rendered]
Date: 2019-08-21T13:35:12.485Z - Hash: 773159a2385d57ce97aa - Time: 9245ms
** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **
ℹ ｢wdm｣: Compiled successfully.
```

### Build

Execute ```ng build``` para build do projeto. Os bundles do build serão armazenados no diretório /dist. Use --prod para build de produção.

## Testing

To run tests:

```
$ npm test

> ng test

 10% building 2/2 modules 0 active21 08 2019 11:18:14.771:WARN [karma]: No captured browser, open http://localhost:9876/
21 08 2019 11:18:14.824:INFO [karma-server]: Karma v4.1.0 server started at http://0.0.0.0:9876/
21 08 2019 11:18:14.824:INFO [launcher]: Launching browsers Chrome with concurrency unlimited
21 08 2019 11:18:14.827:INFO [launcher]: Starting browser Chrome
21 08 2019 11:18:18.866:WARN [karma]: No captured browser, open http://localhost:9876/
21 08 2019 11:18:18.988:INFO [Chrome 76.0.3809 (Mac OS X 10.14.6)]: Connected on socket JJMd7R4AH_JLWXE8AAAA with id 50741919
Chrome 76.0.3809 (Mac OS X 10.14.6): Executed 10 of 10 SUCCESS (0.294 secs / 0.193 secs)
TOTAL: 10 SUCCESS
TOTAL: 10 SUCCESS
```
